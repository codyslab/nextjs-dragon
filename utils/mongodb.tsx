import mongoose from "mongoose";

/**
 * Connect to a MongoDB instance.
 */
export function connectToMongoDb() {
    return mongoose.connect(process.env.MONGODB_CONNECTION_URL!);
}

/**
 * Disconnect from a MongoDB instance.
 */
export function disconnectFromMongoDB() {
    return mongoose.disconnect();
}
