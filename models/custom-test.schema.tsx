import mongoose, { models } from "mongoose";

// Create the test schema for the model.
const CustomTestSchema = new mongoose.Schema({
    test: String
});

// Export the default.
export const CustomTest = models.CustomTest || mongoose.model('CustomTest', CustomTestSchema);