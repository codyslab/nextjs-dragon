## Prerequisites
- You need Docker.
- You need Node 16+.
- Have basic knowledge of NextJS.

## Getting Started

1. Clone this repository.
2. Run `$ npm install` to install the project dependencies.
3. Copy the `.env.example` environment file to `.env.local` at the root of the project. The default `.env.local` file will be sufficient to start your development environment given you are using Docker Compose. If not, you will need to make modifications for your environment. 

## Docker Compose

Execute `$ docker-compose up` to start the development environment. It will automatically build the Docker images you need for your development environment.

## URLs

### Frontend

The frontend can be access via `http://localhost:3000`.

### API

The API can be accessed via `http://localhost:3000/api`. 

Make a GET request to `http://localhost:3000/api/hello` to try it out.

### MailHog

MailHog will be used to intercept emails that will be sent from the API.

MailHog can be accessed via `http://localhost:8025/`.