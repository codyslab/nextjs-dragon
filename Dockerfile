# Base Docker Image.
FROM node:16-alpine

# Copy the crontab file into the container into the root directory.
COPY bin/.crontab /.crontab

# Create and set the working directory.
RUN mkdir -p /usr/src
WORKDIR /usr/src

# Copy the source files into the working directory.
COPY . /usr/src

# Install dependencies.
RUN npm install

# Apply the new crontab.
RUN /usr/bin/crontab /.crontab

# Expose Port 3000
EXPOSE 3000

# Start the CRON worker in the background and the NextJS server in the foreground.
CMD /usr/sbin/crond && npm run dev