import * as cronJSMatcher from '@datasert/cronjs-matcher';
import {NextApiRequest, NextApiResponse} from "next";
import {connectToMongoDb, disconnectFromMongoDB} from "../../utils/mongodb";

/**
 * This endpoint is reserved for running CRON processes.
 * Todo: External requests to this API endpoint should be rejected. It should only run on the server via the crontab.
 * Resources: https://github.com/datasert/cronjs
 * @param req
 * @param res
 */
export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<any>
) {
    try {
        // Connect to MongoDB.
        await connectToMongoDb();

        // Get the current Date ISO string.
        let dateISO = new Date().toISOString();

        // Validate the Date ISO string to a minutely schedule e.g. "* * * * *"
        if ( cronJSMatcher.isTimeMatches('* * * * *', dateISO) ) {
            console.log('MINUTELY CRON SCHEDULE: ', dateISO);
        }

        // Todo: Specify other CRON schedules here.

        // Disconnect from MongoDB.
        await disconnectFromMongoDB();

        // Respond with a success message.
        res.status(200).json({ message: 'The cron executed successfully.' });
    } catch (error: any) {
        // Log the error message.
        console.log('Cron Failed: ' + error.message);
        // Disconnect from MongoDB.
        await disconnectFromMongoDB();
        // Respond with a failure message.
        res.status(200).json({ message: 'The cron failed to execute. ', error: error.message });
    }
}