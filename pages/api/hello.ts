// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { CustomTest } from "../../models/custom-test.schema";
import {connectToMongoDb, disconnectFromMongoDB} from "../../utils/mongodb";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {

  console.log('Attempting to connect to MongoDB, create a document and return all results.');

  try {
    // Connect to MongoDB.
    await connectToMongoDb();

    // Create a test document.
    console.log('Create a test document.');
    const test = new CustomTest({ test: 'Test Document ' + Math.floor(Math.random() * 9999) });
    await test.save();
    console.log('Test Document', test);

    // Get all test models
    console.log('Get all test documents.');
    const tests = await CustomTest.find();
    console.log('Test Documents', tests);

    // Return an API response.
    res.status(200).json({
      test,
      tests
    });

    // Disconnect from MongoDB.
    await disconnectFromMongoDB();
  } catch (error: any) {
    // Log a failed message.
    console.log('Failed: ', error.message);
    // Disconnect from MongoDB.
    await disconnectFromMongoDB();
    // Return an API response.
    res.status(503).json({
      message: error.message
    });
  }

  console.log('Done.');
}
